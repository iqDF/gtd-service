# GTD Service

## Requirements

- `python=3.5.2`
- `MySQL`
- `virutalvenv`
- `git`

## Installation

```bash
pip install -r requirements.txt # ensure that you are using virtualvenv and python 3.7
```

## DB Setup

You need to install `MySQL` DB in the local environment.

After installing, make sure you have created the `gtd` and `gtd_test` database in your local.

```bash
$ mysql -u root -p # Type in your password
mysql> CREATE DATABASE gtd;
mysql> CREATE DATABASE gtd_test;
```

## Run Server

```bash
export DATABASE_URI=mysql+pymysql://root:root@127.0.0.1:3306/gtd # get your mysql url
export FLASK_ENV=development # dev mode
export DEBUG=True # dev mode
python run.py # available at 127.0.0.1:5000
```

## Run Testing

```bash
pytest -s -v
```
