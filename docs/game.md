# `Mass Game API`

## Notes

```python
# Error Codes
ServerOk = 0
ServerError = 1
DatabaseError = 2
OperationNotSupported = 3
```

## `POST api/user/login`

Retrieve user details

Request

```json
{
  "password": "Aksajdkf12soaif"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "user": {
    "id": 1,
    "name": "OG - ...",
    "description": "...",
    "score": 0
  }
}
```

## `GET api/question`

Retrieve the question details

Request parameter

```
id: String
user_id: String
```

Error Code

```
0: No Error
1: Question is not available
```

Error Response

```json
{
  "result": false,
  "error_code": 1,
  "error_message": "SEVER_ERROR"
}
```

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "question": {
    "id": 1,
    "question_text": "Lorem unpo ...",
    "choices": [
      { "option": "A", "text": "A is the answer" },
      { "option": "B", "text": "B is the answer" },
      { "option": "C", "text": "C is the answer" },
      { "option": "D", "text": "D is the answer" }
    ]
  }
}
```

## `POST api/question/{question_id}`

To answer a question

Request

```json
{
  "user_id": 2,
  "option": "A"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "user": {
    "id": 1,
    "name": "OG - ...",
    "description": "...",
    "score": 1
  }
}
```
