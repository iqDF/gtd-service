# `Admin API`

## Notes

```python
# Error Codes
ServerOk = 0
SERVER_ERROR = 1
DatabaseError = 2
OperationNotSupported = 3
```

## `GET api/user/list`

Retrieve the user list

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "user": [
    {
      "id": 1,
      "name": "...",
      "password": "...",
      "description": "...",
      "score": 0
    }
  ]
}
```

## `POST api/user/create`

Create user

Request

```json
{
  "name": "GTD Admin",
  "password": "...",
  "description": "...",
  "score": 0
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "user": {
    "id": 1,
    "name": "GTD Admin",
    "password": "...",
    "description": "...",
    "score": 0
  }
}
```

## `PUT api/user/<user_id>/update`

Update user

Request

```json
{
  "name": "GTD Admin 2",
  "description": "New desc here"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "user": {
    "id": 1,
    "name": "GTD Admin 2",
    "password": "...",
    "description": "New desc here",
    "score": 0
  }
}
```

## `DELETE api/user/<user_id>/delete`

Delete user

Response

```json
{
  "result": true
}
```

## `GET api/question/list`

Retrieve the question list

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "question": [
    {
      "id": 1,
      "question_text": "Lorem unpo ...",
      "is_opened": false,
      "user_answer": null,
      "choices": [
        { "id": 1, "option": "A", "text": "A is the answer", "is_answer": true },
        { "id": 2, "option": "B", "text": "B is the answer" },
        { "id": 3, "option": "C", "text": "C is the answer" },
        { "id": 4, "option": "D", "text": "D is the answer" }
      ]
    }
  ]
}
```

## `POST api/question/create`

To create a question

Request

```json
{
  "user_id": 2,
  "question_text": "Question text here",
  "choices": [
    { "id": 1, "option": "A", "text": "A is the answer", "is_answer": true },
    { "id": 2, "option": "B", "text": "B is the answer" },
    { "id": 3, "option": "C", "text": "C is the answer" },
    { "id": 4, "option": "D", "text": "D is the answer" }
  ]
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "question": {
    "id": 1,
    "question_text": "Question text here",
    "is_opened": false,
    "user_answer": null,
    "choices": [
      { "id": 1, "option": "A", "text": "A is the answer", "is_answer": true },
      { "id": 2, "option": "B", "text": "B is the answer" },
      { "id": 3, "option": "C", "text": "C is the answer" },
      { "id": 4, "option": "D", "text": "D is the answer" }
    ]
  }
}
```

## `PUT api/question/<question_id>/update`

To update a question

Request

```json
{
  "question_text": "New Question Text here"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "question": {
    "id": 1,
    "question_text": "New Question Text here",
    "is_opened": false,
    "user_answer": null,
    "choices": [
      { "id": 1, "option": "A", "text": "A is the answer", "is_answer": true },
      { "id": 2, "option": "B", "text": "B is the answer" },
      { "id": 3, "option": "C", "text": "C is the answer" },
      { "id": 4, "option": "D", "text": "D is the answer" }
    ]
  }
}
```

## `DELETE api/question/<question_id>/delete`

Delete question

Response

```json
{
  "result": true
}
```

## `PUT api/question/<question_id>/reset`

Reset question, will set `is_opened` to `false` and `user_answer` to `null`

Response

```json
{
  "result": true,
  "error_code": 0,
  "question": {
    "id": 1,
    "question_text": "New Question Text here",
    "is_opened": false,
    "user_answer": null,
    "choices": [
      { "id": 1, "option": "A", "text": "A is the answer", "is_answer": true },
      { "id": 2, "option": "B", "text": "B is the answer" },
      { "id": 3, "option": "C", "text": "C is the answer" },
      { "id": 4, "option": "D", "text": "D is the answer" }
    ]
  }
}
```

## `GET api/level/list`

Retrieve the level list

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "level": [
    {
      "id": 1,
      "password": "password",
      "bg_color": "#000000",
      "text_color": "#000000",
      "name": "name",
      "destination": { "lat": 1.23, "lng": 1.23, "name": "gtd" },
      "schedule": { "start_time": "11:00", "end_time": "11:00" },
      "guideline": "guideline"
    }
  ]
}
```

## `POST api/question/create`

To create a question

Request

```json
{
  "password": "password",
  "bg_color": "#000000",
  "text_color": "#000000",
  "name": "name",
  "destination": { "lat": 1.23, "lng": 1.23, "name": "gtd" },
  "schedule": { "start_time": "11:00", "end_time": "11:00" },
  "guideline": "guideline"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "level": {
    "id": 1,
    "password": "password",
    "bg_color": "#000000",
    "text_color": "#000000",
    "name": "name",
    "destination": { "lat": 1.23, "lng": 1.23, "name": "gtd" },
    "schedule": { "start_time": "11:00", "end_time": "11:00" },
    "guideline": "guideline"
  }
}
```

## `PUT api/level/<level_id>/update`

To update a level

Request

```json
{
  "password": "new password"
}
```

Response

```json
{
  "result": true,
  "error_code": 0,
  "level": {
    "id": 1,
    "password": "new password",
    "bg_color": "#000000",
    "text_color": "#000000",
    "name": "name",
    "destination": { "lat": 1.23, "lng": 1.23, "name": "gtd" },
    "schedule": { "start_time": "11:00", "end_time": "11:00" },
    "guideline": "guideline"
  }
}
```

## `DELETE api/level/<level_id>/delete`

Delete level

Response

```json
{
  "result": true
}
```
