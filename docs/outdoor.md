# `Mass Game API`

## Notes

## `GET api/level`

Retrieve the outdoor level list

Request Parameter

```
user_id: int
```

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "levels": [
    {
      "id": 1,
      "name": "Level #1",
      "bg_color": "#34aa44",
      "text_color": "#ffffff"
    },
    {
      "id": 2,
      "name": "Level #2",
      "bg_color": "#facf55",
      "text_color": "#ffffff"
    },
    {
      "id": 3,
      "name": "Level #3",
      "bg_color": "#e6492d",
      "text_color": "#ffffff"
    }
  ]
}
```

## `POST api/level/{level_id}`

Retrieve the outdoor level detail

Request

```json
{
  "user_id": 20,
  "password": "__password_here__"
}
```

Success Response

```json
{
  "result": true,
  "error_code": 0,
  "level": {
    "id": 1,
    "name": "Level #1",
    "hidden_mission": ["Buat 1 OG nyanyi bagaikan girlband/boyband", "Seluruh angora OG berdiri dengan satu kaki"],
    "destination": { "lat": 1.347707, "lng": 103.678387, "name": "NIE" },
    "schedule": { "start_time": "9:30", "end_time": "11:30" },
    "guideline": "<p>From NTU, take bus 179 to Pioneer / Boon Lay MRT Station.</p>
<p>Take MRT <span style='color: green'>green line (to Changi)</span> to Buona Vista station and change to circle line.</p>
<p>Take MRT <span style='color: yellow'>circle line (to Harbourfront)</span> to Haw Par Villa.</p>
<p>The entrance is just outside the exit.</p>
<p>Find the officers near the Ten Courts of Hell.</p>" // in html format
  }
}
```
