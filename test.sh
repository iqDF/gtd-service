#!/bin/bash

# get your mysql url for test
export DATABASE_URI=mysql+pymysql://root:root@127.0.0.1:3306/gtd_test 
export FLASK_ENV=development
export DEBUG=True
pytest -s -v