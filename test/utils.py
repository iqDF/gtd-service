from app import db
from app.game.models.game_models import (
    User,
    Question,
    Choice,
)
from app.outdoor.models.outdoor_models import (
    Level,
    LevelDetail,
    UserLevel,
)
from test.data import (
    create_user_data,
    create_admin_data,
    create_question_data,
    create_choice_data,
    create_correct_choice_data,
    create_level_data,
    create_level_detail_data,
)


def create_user_db():
    # Create user
    user_data = create_user_data()
    user = User(**user_data)
    db.session.add(user)
    db.session.commit()
    return user


def create_admin_db():
    # Create admin
    admin_data = create_admin_data()
    admin = User(**admin_data)
    db.session.add(admin)
    db.session.commit()
    return admin


def create_question_db(choice_data=create_choice_data()):
    # Create question
    question_data = create_question_data()
    question = Question(**question_data)
    db.session.add(question)
    db.session.commit()
    # Create choice
    choice = Choice(question_id=question.id, **choice_data)
    db.session.add(choice)
    db.session.commit()
    # Form the question + choice data
    question.choices = [choice]
    return question


def clean_choices_key(choices):
    [
        choice.pop(key, None)
        for key in ['id', 'question_id', 'is_answer']
        for choice in choices
    ]
    return choices


def create_level_db():
    # Create Level
    level_data = create_level_data()
    level = Level(**level_data)
    db.session.add(level)
    db.session.commit()
    # Create level detail
    level_detail_data = create_level_detail_data()
    level_detail = LevelDetail(level_id=level.id, **level_detail_data)
    db.session.add(level_detail)
    db.session.commit()
    level.detail = level_detail.to_dict()
    return level
