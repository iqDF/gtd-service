from test.utils import (
    create_user_db,
    create_level_db,
)
from app import db

# ------------------------
# LEVEL
# ------------------------


def test_get_levels(client, get, init_user_db, init_level_db):
    created_user = create_user_db()

    data, resp = get(
        client,
        '/api/level',
        dict(user_id=created_user.id),
    )
    level = data['level']

    assert resp.status_code == 200
    assert isinstance(level, list)
    assert len(level) == 3


def test_get_level_detail(client, post, init_user_db, init_level_db):
    created_user = create_user_db()
    created_level = create_level_db()

    user_id = created_user.id
    level_id = created_level.id
    created_detail = created_level.detail

    data, resp = post(
        client,
        '/api/level/%s' % level_id,
        dict(
            level_id=str(level_id),
            user_id=str(user_id),
            password=created_level.password,
        )
    )
    level = data['level']

    assert resp.status_code == 200
    assert isinstance(level, dict)

    assert level['hidden_mission'] == created_detail['hidden_mission']
    assert level['destination'] == created_detail['destination']
    assert level['schedule'] == created_detail['schedule']
    assert level['guideline'] == created_detail['guideline']
