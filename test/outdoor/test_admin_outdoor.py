from test.utils import (
    create_user_db,
    create_level_db,
)
from test.data import (
    create_level_data,
    create_level_detail_data,
)
from app import db

# ------------------------
# LEVEL
# ------------------------


def test_get_level_list(client, get, init_level_db):
    data, resp = get(
        client,
        '/api/level/list',
        admin=True,
    )
    level = data['level']

    assert resp.status_code == 200
    assert isinstance(level, list)
    assert len(level) == 3


def test_create_level(client, post, init_user_db, init_level_db):
    level_data = create_level_data()
    level_detail_data = create_level_detail_data()

    data, resp = post(
        client,
        '/api/level/create',
        dict(**level_data, **level_detail_data),
        admin=True,
    )
    level = data['level']

    assert resp.status_code == 200
    assert isinstance(level, dict)

    assert level['password'] == level_data['password']
    assert level['bg_color'] == level_data['bg_color']
    assert level['text_color'] == level_data['text_color']
    assert level['name'] == level_data['name']
    assert level['destination'] == level_detail_data['destination']
    assert level['schedule'] == level_detail_data['schedule']
    assert level['guideline'] == level_detail_data['guideline']


def test_delete_level(client, delete, init_user_db, init_level_db):
    level_data = create_level_db()

    data, resp = delete(
        client,
        '/api/level/%d/delete' % level_data.id,
        admin=True,
    )
    level = data['level']

    assert resp.status_code == 200
    assert isinstance(level, dict)
