from random import randint


def rand():
    return randint(10, 1000)


def create_user_data():
    return {
        'name': 'Mock User %s' % rand() ,
        'password': 'Mock User %s' % rand() ,
        'description': 'Mock User %s' % rand() ,
        'score': 0,
        'is_admin': False,
    }

def create_admin_data():
    return {
        'name': 'Mock Admin User %s' % rand() ,
        'password': 'Mock Admin User %s' % rand() ,
        'description': 'Mock Admin User %s' % rand() ,
        'score': 0,
        'is_admin': True,
    }


def create_question_data():
    return {
        'user_id': 1,
        'question_text': 'Mock Question %s' % rand() ,
    }


def create_choice_data():
    return {
        'option': 'Mock Option %s' % rand() ,
        'text': 'Mock Choice %s' % rand() ,
    }


def create_correct_choice_data():
    return {
        'option': 'Mock Option %s' % rand() ,
        'text': 'Mock Choice %s' % rand() ,
        'is_answer': True
    }


def create_level_data():
    return {
        'password': 'Mock password %s' % rand() ,
        'name': 'Mock name %s' % rand() ,
        'bg_color': '#123457',
        'text_color': '#123457',
    }


def create_level_detail_data():
    return {
        'hidden_mission': ['Hidden Mission'],
        'destination': {'lat': 15.2213, 'lng': 1.31251, 'name': 'Dest'},
        'schedule': {
            'start_time': '11:00',
            'end_time': '12:00',
        },
        'guideline': 'Mock guideline %s' % rand() ,
    }
