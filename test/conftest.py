from test.utils import (
    create_user_db,
    create_admin_db,
    create_question_db,
    create_level_db,
)
from urllib.parse import urlencode
from flask import json
from functools import wraps
import pytest


@pytest.fixture
def client():
    from app import flask_app, db

    yield flask_app


def get_url(url, query):
    if isinstance(query, dict):
        return url + '?' + urlencode(query)
    return url


def test_client_call(method, app, url, admin, query=None, payload=None):
    params = {'content_type': 'application/json'}
    headers = {}
    if payload:
        params.update(dict(data=json.dumps(payload)))
    if admin:
        admin = create_admin_db()
        headers = {'admin_id': admin.id}
    final_url = get_url(url, query)
    resp = getattr(app.test_client(), method)(
        final_url, **params, headers=headers)
    data = json.loads(resp.get_data(as_text=True))
    return data, resp


@pytest.fixture
def get():
    def get_wrapper(app, url, query=None, admin=False):
        return test_client_call(method='get', **locals())
    return get_wrapper


@pytest.fixture
def post():
    def post_wrapper(app, url, payload, query=None, admin=False):
        return test_client_call(method='post', **locals())
    return post_wrapper


@pytest.fixture
def put():
    def put_wrapper(app, url, payload, query=None, admin=False):
        return test_client_call(method='put', **locals())
    return put_wrapper


@pytest.fixture
def delete():
    def delete_wrapper(app, url, query=None, admin=False):
        return test_client_call(method='delete', **locals())
    return delete_wrapper


def pytest_sessionfinish(session, exitstatus):
    from app import db
    db.session.remove()
    db.drop_all()


@pytest.fixture
def init_db(client):
    from app import db
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


# Game DB Init


@pytest.fixture
def init_user_db(init_db):
    [create_user_db() for i in range(3)]


@pytest.fixture
def admin_id(init_db):
    return create_admin_db().id


@pytest.fixture
def init_question_db(init_db):
    [create_question_db() for i in range(3)]


@pytest.fixture
def init_level_db(init_db):
    [create_level_db() for i in range(3)]
