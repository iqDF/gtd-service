from test.utils import (
    create_user_db,
    create_question_db,
    clean_choices_key as clean_choices,
)
from test.data import (
    create_user_data,
    create_question_data,
    create_choice_data,
)
from app import db
from app.game.models.game_models import Question


# ------------------------
# USER
# ------------------------

def test_get_user_list(client, get, init_user_db):
    data, resp = get(client, '/api/user/list', admin=True)
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, list)
    assert len(user) == 4  # 3 normal + 1 admin


def test_create_user(client, post, init_user_db):
    created_user = create_user_data()

    data, resp = post(client, '/api/user/create', created_user, admin=True)
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)
    assert user['name'] == created_user['name']
    assert user['description'] == created_user['description']


def test_update_user(client, put, init_user_db):
    created_user = create_user_db()
    user_id = created_user.id
    new_desc = 'New Desc'

    data, resp = put(
        client,
        '/api/user/%s/update' % user_id,
        {'description': new_desc},
        admin=True,
    )
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)
    assert user['name'] == created_user.name
    assert user['description'] == new_desc


def test_delete_user(client, delete, init_user_db, init_question_db):
    user_data = create_user_db()

    data, resp = delete(
        client,
        '/api/user/%s/delete' % 1,
        admin=True,
    )
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)


# ------------------------
# QUESTION
# ------------------------


def test_get_question_list(client, get, init_user_db, init_question_db):
    data, resp = get(client, '/api/question/list', admin=True)
    question = data['question']

    assert resp.status_code == 200
    assert isinstance(question, list)
    assert len(question) == 3


def test_create_question(client, post, init_user_db, init_question_db):
    created_question = create_question_data()
    created_choices = [create_choice_data()]
    param = {**created_question, 'choices': created_choices}

    data, resp = post(client, '/api/question/create', param, admin=True)
    question = data['question']

    assert resp.status_code == 200
    assert isinstance(question, dict)
    assert question['user_id'] == created_question['user_id']
    assert question['question_text'] == created_question['question_text']
    assert clean_choices(question['choices']) == created_choices


def test_update_question(client, put, init_user_db, init_question_db):
    created_user = create_user_db()
    created_question = create_question_db()
    question_id = created_question.id
    user_id = created_question.user_id
    first_choice_id = created_question.choices[0].id

    new_question_text = 'New Question Text'
    new_user_id = created_user.id
    new_choice_data = create_choice_data()
    new_choices_obj = [dict(id=first_choice_id, **new_choice_data)]
    param = dict(
        question_text=new_question_text,
        user_id=new_user_id,
        choices=new_choices_obj,
    )
    data, resp = put(
        client,
        '/api/question/%s/update' % question_id,
        param,
        admin=True,
    )
    question = data['question']

    assert resp.status_code == 200
    assert isinstance(question, dict)
    assert question['question_text'] == new_question_text
    assert question['user_id'] == new_user_id
    assert clean_choices(question['choices']) == clean_choices(new_choices_obj)


def test_delete_question(client, delete, init_user_db, init_question_db):
    question_data = create_question_db()

    data, resp = delete(
        client,
        '/api/question/%s/delete' % question_data.id,
        admin=True,
    )
    question = data['question']

    assert resp.status_code == 200
    assert isinstance(question, dict)
