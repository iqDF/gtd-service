from test.utils import (
    create_user_db,
    create_question_db,
)
from test.data import (
    create_correct_choice_data,
)
from app import db

# ------------------------
# USER
# ------------------------


def test_get_user_login(client, post, init_user_db):
    created_user = create_user_db()

    data, resp = post(
        client,
        '/api/user/login',
        dict(password=created_user.password),
    )
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)
    assert user['id'] == created_user.id
    assert user['name'] == created_user.name
    assert user['description'] == created_user.description
    assert user['score'] == created_user.score
    assert 'password' not in user.keys()


# ------------------------
# QUESTION
# ------------------------


def test_get_question(client, get, init_user_db, init_question_db):
    created_question = create_question_db()
    created_choices = [choice.to_dict() for choice in created_question.choices]

    data, resp = get(
        client,
        '/api/question',
        dict(user_id=created_question.user_id, id=created_question.id)
    )
    question = data['question']

    assert resp.status_code == 200
    assert isinstance(question, dict)
    assert question['question_text'] == created_question.question_text
    assert question['choices'] == created_choices


def test_answer_question_correct_choice(client, post, init_user_db, init_question_db):
    created_question = create_question_db(create_correct_choice_data())
    created_choices = [choice.to_dict() for choice in created_question.choices]
    option = created_choices[0]['option']

    data, resp = post(
        client,
        '/api/question/%s' % created_question.id,
        dict(user_id=created_question.user_id, option=option)
    )
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)
    assert user['score'] == 1


def test_answer_question_wrong_choice(client, post, init_user_db, init_question_db):
    created_question = create_question_db()
    created_choices = [choice.to_dict() for choice in created_question.choices]
    option = created_choices[0]['option']

    data, resp = post(
        client,
        '/api/question/%s' % created_question.id,
        dict(user_id=created_question.user_id, option=option)
    )
    user = data['user']

    assert resp.status_code == 200
    assert isinstance(user, dict)
    assert user['score'] == 0
