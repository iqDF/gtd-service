from flask import Blueprint, jsonify, request

from app.outdoor.libs import (
    outdoor_manager,
)

from app.bridge.constants.error import get_result_status_code, get_msg_error

from app.outdoor.views.outdoor_validators import (
    get_levels_validator,
    get_level_detail_validator,
)
mod_outdoor = Blueprint('outdoor', __name__)


# ------------------------
# LEVEL
# ------------------------


@mod_outdoor.route('/api/level', methods=['GET'])
@get_levels_validator
def api_get_levels():
    query = dict(request.args)
    error_code, levels = outdoor_manager.get_levels(
        user_id=int(query['user_id'][0]))
    return jsonify({
        **get_msg_error(error_code),
        'level': levels,
    })


@mod_outdoor.route('/api/level/<int:level_id>', methods=['POST'])
@get_level_detail_validator
def api_get_level_detail(level_id):
    payload = dict(request.json)

    error_code, detail = outdoor_manager.get_level_detail(
        level_id=level_id,
        user_id=int(payload['user_id']),
        password=payload['password']
    )
    return jsonify({
        **get_msg_error(error_code),
        'level': detail
    })
