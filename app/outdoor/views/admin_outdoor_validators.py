from app.bridge.decorators.validators import request_validator, assert_key, assert_length


@request_validator
def get_level_list_validator():
    pass


@request_validator
def create_level_validator(payload):
    assert_key(payload, 'password', str)
    assert_key(payload, 'name', str)

    # Check color data structure
    assert_key(payload, 'bg_color', str)
    assert_key(payload, 'text_color', str)
    assert_length(payload, 'bg_color', 7)
    assert_length(payload, 'text_color', 7)

    # Check hidden mission data structure
    assert_key(payload, 'hidden_mission', list)
    hidden_mission = payload['hidden_mission']
    for i in range(len(payload['hidden_mission'])):
        assert_key(hidden_mission, i, str)

    # Check destination data structure
    assert_key(payload, 'destination', dict)
    destination = payload['destination']
    assert_key(destination, 'lat', float)
    assert_key(destination, 'lng', float)
    assert_key(destination, 'name', str)

    # Check schedule data structure
    assert_key(payload, 'schedule', dict)
    schedule = payload['schedule']
    assert_key(schedule, 'start_time', str)
    assert_length(schedule, 'start_time', 5)
    assert_key(schedule, 'end_time', str)
    assert_length(schedule, 'end_time', 5)

    assert_key(payload, 'guideline', str)
