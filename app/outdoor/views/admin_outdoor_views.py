from flask import Blueprint, jsonify, request

from app.outdoor.libs import (
    outdoor_manager,
)

from app.bridge.decorators.admin_required import admin_required

from app.bridge.constants.error import get_result_status_code, get_msg_error

from app.outdoor.views.admin_outdoor_validators import (
    get_level_list_validator,
    create_level_validator,
)

mod_admin_outdoor = Blueprint('admin_outdoor', __name__)


# ------------------------
# LEVEL
# ------------------------


@mod_admin_outdoor.route('/api/level/list', methods=['GET'])
@admin_required
@get_level_list_validator
def api_get_level_list():
    error_code, level = outdoor_manager.get_all_level()
    return jsonify({
        **get_msg_error(error_code),
        'level': level
    })


@mod_admin_outdoor.route('/api/level/create', methods=['POST'])
@admin_required
@create_level_validator
def api_create_level():
    payload = dict(request.json)
    error_code, level = outdoor_manager.create_level(
        password=payload['password'],
        name=payload['name'],
        bg_color=payload['bg_color'],
        text_color=payload['text_color'],
        hidden_mission=payload['hidden_mission'],
        destination=payload['destination'],
        schedule=payload['schedule'],
        guideline=payload['guideline']
    )
    return jsonify({
        **get_msg_error(error_code),
        'level': level
    })


@mod_admin_outdoor.route('/api/level/<int:level_id>/update', methods=['PUT'])
@admin_required
@create_level_validator
def api_update_level(level_id):
    payload = dict(request.json)
    error_code, level = outdoor_manager.update_level(
        level_id=level_id,
        password=payload['password'],
        name=payload['name'],
        bg_color=payload['bg_color'],
        text_color=payload['text_color'],
        hidden_mission=str(payload['hidden_mission']),
        destination=str(payload['destination']),
        schedule=str(payload['schedule']),
        guideline=payload['guideline']
    )
    return jsonify({
        **get_msg_error(error_code),
        'level': level
    })


@mod_admin_outdoor.route('/api/level/<int:level_id>/delete', methods=['DELETE'])
@admin_required
def api_delete_level(level_id):
    error_code, level = outdoor_manager.delete_level(level_id)
    return jsonify({
        **get_msg_error(error_code),
        'level': level
    })
