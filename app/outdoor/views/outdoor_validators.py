from app.bridge.decorators.validators import request_validator, assert_key


@request_validator
def get_levels_validator(query):
    assert_key(query, 'user_id', [str, int])


@request_validator
def get_level_detail_validator(payload):
    assert_key(payload, 'user_id', [str, int])
    assert_key(payload, 'password', str)
