from sqlalchemy import and_
from app import db
from app.outdoor.models.outdoor_models import (
    Level,
    LevelDetail,
    UserLevel,
)
from app.bridge.decorators.db_exception import (
    db_exception
)
from app.bridge.constants.error import (
    OperationNotSupported
)

# ------------------------
# LEVEL
# ------------------------
@db_exception
def get_levels(password):
    return [level.to_dict(password) for level in Level.query.all()]


def get_level_by_id(level_id, password):
    return Level.query.filter_by(id=level_id).first().to_dict(password)


@db_exception
def create_level(name, password, bg_color, text_color):
    level = Level(**locals())
    db.session.add(level)
    db.session.commit()
    return level.to_dict(True)


@db_exception
def update_level(level_id, data):
    level = Level.query.filter_by(
        id=level_id
    )
    level.update(data)
    db.session.commit()
    return level.first().to_dict(True)


@db_exception
def delete_level_by_id(level_id):
    level = Level.query.filter_by(id=level_id).first()
    db.session.delete(level)
    db.session.commit()
    return level.to_dict(True)


# ------------------------
# USER LEVEL
# ------------------------

@db_exception
def get_user_levels(user_id):
    return [user_level.to_dict() for user_level in UserLevel.query.filter_by(user_id=user_id)]


@db_exception
def add_user_levels(user_id, level_id):
    user_level = UserLevel(user_id, level_id)
    db.session.add(user_level)
    db.session.commit()


# ------------------------
# LEVEL DETAIL
# ------------------------


@db_exception
def get_level_detail(level_id):
    return LevelDetail.query.filter_by(
        level_id=level_id
    ).first().to_dict()


@db_exception
def create_level_details(level_id, hidden_mission, destination, schedule, guideline):
    details = LevelDetail(**locals())
    db.session.add(details)
    db.session.commit()
    return details.to_dict()


@db_exception
def update_level_detail(level_id, data):
    details = LevelDetail.query.filter_by(
        level_id=level_id
    )
    details.update(data)
    db.session.commit()
    return details.first().to_dict()
