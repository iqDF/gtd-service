from app.outdoor.data_access import (
    outdoor_model_manager
)
from app.bridge.constants.error import ServerOk, DatabaseError, OperationNotSupported

# ------------------------
# LEVEL PUBLIC
# ------------------------


def get_levels(user_id=None, password=False):
    levels = outdoor_model_manager.get_levels(password)
    if isinstance(levels, DatabaseError):
        return levels, []
    if not user_id:
        return ServerOk(), levels
    else:
        unlocked_user_levels = get_unlocked_levels(user_id)
        for level in levels:
            if level['id'] in unlocked_user_levels:
                level.update(dict(locked=False))
            else:
                level.update(dict(locked=True))
        return ServerOk(), levels


def get_unlocked_levels(user_id):
    user_levels = outdoor_model_manager.get_user_levels(user_id)
    unlocked_user_levels = [ul['level_id'] for ul in user_levels]
    return unlocked_user_levels


# ------------------------
# LEVEL ADMIN
# ------------------------


def get_all_level():
    levels = outdoor_model_manager.get_levels(password=True)
    if isinstance(levels, DatabaseError):
        return levels, []
    data = []
    for level in levels:
        level_detail = outdoor_model_manager.get_level_detail(level['id'])
        data.append({**level_detail, **level})
    return ServerOk(), data


def get_level_detail(level_id, user_id, password):
    level = outdoor_model_manager.get_level_by_id(level_id, True)
    details = outdoor_model_manager.get_level_detail(
        level_id=level_id,
    )
    if isinstance(details, DatabaseError):
        return details, {}
    # If user_id hasn't unlocked it before, we need to check the password is correct or not
    details = {**details, **level}
    if level_id in get_unlocked_levels(user_id):
        return ServerOk(), details
    if level['password'] != str(password):
        return OperationNotSupported('Incorrect password'), {}
    outdoor_model_manager.add_user_levels(user_id, level_id)
    return ServerOk(), details


def create_level(name, password, bg_color, text_color, hidden_mission, destination, schedule, guideline):
    level = outdoor_model_manager.create_level(
        name=name,
        password=password,
        bg_color=bg_color,
        text_color=text_color,
    )
    if isinstance(level, DatabaseError):
        return level, {}
    details = outdoor_model_manager.create_level_details(
        level_id=level['id'],
        hidden_mission=hidden_mission,
        destination=destination,
        schedule=schedule,
        guideline=guideline,
    )
    if isinstance(details, DatabaseError):
        return details, {}
    level = {**level, **details}
    return ServerOk(), level


def update_level(
    level_id,
    password,
    name,
    bg_color,
    text_color,
    hidden_mission,
    destination,
    schedule,
    guideline,
):
    level = outdoor_model_manager.update_level(level_id, dict(
        name=name,
        password=password,
        bg_color=bg_color,
        text_color=text_color,
    ))
    if isinstance(level, DatabaseError):
        return level, {}
    level_detail = outdoor_model_manager.update_level_detail(level_id, dict(
        hidden_mission=hidden_mission,
        destination=destination,
        schedule=schedule,
        guideline=guideline,
    ))
    if isinstance(level_detail, DatabaseError):
        return level_detail, {}
    level = {**level_detail, **level}
    return ServerOk(), level


def delete_level(level_id):
    level = outdoor_model_manager.delete_level_by_id(level_id)
    if isinstance(level, DatabaseError):
        return level, 0
    return ServerOk(), level
