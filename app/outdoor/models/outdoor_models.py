from app import db


class Level(db.Model):
    __tablename__ = 'level'

    id = db.Column(db.Integer(), primary_key=True)
    password = db.Column(db.String(100), unique=True)
    name = db.Column(db.String(80))
    bg_color = db.Column(db.String(80))
    text_color = db.Column(db.String(80))

    def __init__(self, name, bg_color, text_color, password):
        self.name = name
        self.bg_color = bg_color
        self.text_color = text_color
        self.password = password

    def to_dict(self, password=False):
        base = {
            'id': self.id,
            'name': self.name,
            'bg_color': self.bg_color,
            'text_color': self.text_color,
        }
        if password:
            base.update(dict(password=self.password))
        return base


class LevelDetail(db.Model):
    __tablename__ = 'level_detail'

    id = db.Column(db.Integer(), primary_key=True)
    level_id = db.Column(db.Integer, db.ForeignKey(
        'level.id', onupdate='SET NULL', ondelete='CASCADE'
    ), nullable=True)
    hidden_mission = db.Column(db.String(1000))
    destination = db.Column(db.String(255))
    schedule = db.Column(db.String(255))
    guideline = db.Column(db.String(3000))

    def __init__(self, level_id, hidden_mission, destination, schedule, guideline):
        self.level_id = level_id
        self.hidden_mission = str(hidden_mission)
        self.destination = str(destination)
        self.schedule = str(schedule)
        self.guideline = guideline

    def to_dict(self):
        return {
            'id': self.id,
            'level_id': self.level_id,
            'hidden_mission': eval(self.hidden_mission),
            'destination': eval(self.destination),
            'schedule': eval(self.schedule),
            'guideline': self.guideline
        }


class UserLevel(db.Model):
    __tablename__ = 'user_level'

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(
        'user.id', onupdate='SET NULL', ondelete='CASCADE'
    ), nullable=True)
    level_id = db.Column(db.Integer, db.ForeignKey(
        'level.id', onupdate='SET NULL', ondelete='CASCADE'
    ), nullable=True)

    def __init__(self, user_id, level_id):
        self.user_id = user_id
        self.level_id = level_id

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'level_id': self.level_id,
        }
