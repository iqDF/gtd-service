class ServerOk:
    error_code = 0


class Error:
    message = ''
    error_code = 1

    def __init__(self, message):
        self.message = message


class DatabaseError(Error):
    error_code = 2


class OperationNotSupported(Error):
    error_code = 3


class InvalidRequest(Error):
    error_code = 99


def get_result_msg(error_code):
    if isinstance(error_code, ServerOk):
        return ''
    if isinstance(error_code, Error):
        return error_code.message
    return None


def get_result_bool(error_code):
    if isinstance(error_code, ServerOk):
        return True
    return False


def get_result_bool(error_code):
    if isinstance(error_code, ServerOk):
        return True
    return False


def get_result_status_code(error_code):
    if isinstance(error_code, ServerOk):
        return 200
    return 400


def get_msg_error(error_code, *args, **kwargs):
    return {
        'result': get_result_bool(error_code),
        'error_message': get_result_msg(error_code, *args, **kwargs),
        'error_code': error_code.error_code,
    }
