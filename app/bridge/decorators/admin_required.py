from functools import wraps
from flask import jsonify, request
from app import flask_app
from app.bridge.constants.error import get_msg_error, get_result_status_code, InvalidRequest, ServerOk
from app.game.libs import game_manager
from app.bridge.constants.error import ServerOk


def admin_required(func):
    @wraps(func)
    def _func(*args, **kwargs):
        headers = dict(request.headers)
        if 'Admin-Id' in headers:
            err, user = game_manager.get_user_admin_by_id(headers['Admin-Id'])
            if isinstance(err, ServerOk):
                return func(*args, **kwargs)
        return jsonify(get_msg_error(InvalidRequest("Admin is required")))
    return _func
