import inspect
from functools import wraps
from flask import jsonify, request
from app import flask_app
from app.bridge.constants.error import get_msg_error, get_result_status_code, InvalidRequest


def request_validator(validator):
    """
    a decorator that will convert normal function into a decorator that can be used to validate request in api.py
    """
    def __func(func):
        @wraps(func)
        def _func(*args, **kwargs):
            try:
                param = {}
                validator_args = inspect.getfullargspec(validator).args
                if 'payload' in validator_args:
                    param.update(dict(payload=request.json))
                if 'query' in validator_args:
                    param.update(dict(query=request.args))
                validator(**param)
            except Exception as e:
                flask_app.logger.error(
                    'validation error: `%s` | request: `%s`',
                    str(e), str(request)
                )
                return jsonify(get_msg_error(InvalidRequest(str(e))))
            return func(*args, **kwargs)
        return _func
    return __func


def assert_key(data, key, var_type, optional=False):
    """
    check whether key is in data and it is in the correct data type
    allow the key to not be in the data if optional is True
    """
    if optional is True and key not in data:
        return

    if not isinstance(data, list):
        key_required_msg = 'field `%s` is required' % key
        assert key in data, key_required_msg

    type_error_msg = 'field `%s` expected to be `%s` but received `%s`' % (
        key, var_type, type(data[key]))
    if isinstance(var_type, list):
        assert type(data[key]) in var_type, type_error_msg
    else:
        assert isinstance(data[key], var_type), type_error_msg


def assert_length(data, key, length, optional=False):
    if optional is True and key not in data:
        return

    key_required_msg = 'field `%s` is required' % key
    assert key in data, key_required_msg

    length_error_msg = 'field `%s` expected to be `%s` length but received `%s`' % (
        key,
        length,
        len(data[key])
    )
    assert len(data[key]) == length, length_error_msg
