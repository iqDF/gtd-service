
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import logging
import os

flask_app = Flask(__name__)
flask_app.config.from_object('config.flask_config')
db = SQLAlchemy(flask_app)


def setup_db():
    from app.game.models import game_models
    from app.outdoor.models import outdoor_models

    db.create_all()


def setup_logging():
    print("Setting up logging")
    if not os.path.isdir("logs"):
        os.makedirs('logs')

    formatter = logging.Formatter("[%(asctime)s] %(message)s")

    handler_info = TimedRotatingFileHandler(
        './logs/info.log', when='midnight', interval=1, backupCount=5)
    handler_info.setLevel(logging.INFO)
    handler_info.setFormatter(formatter)

    handler_error = TimedRotatingFileHandler(
        './logs/error.log', when='midnight', interval=1, backupCount=5)
    handler_error.setLevel(logging.ERROR)
    handler_error.setFormatter(formatter)

    # access log
    logger = logging.getLogger('werkzeug')
    handler_access = logging.FileHandler('./logs/access.log')

    flask_app.logger.addHandler(handler_info)
    flask_app.logger.addHandler(handler_error)
    logger.addHandler(handler_access)


def get_register_blueprints():
    # Admin API
    from app.game.views.admin_game_views import mod_admin_game
    from app.outdoor.views.admin_outdoor_views import mod_admin_outdoor
    # User API
    from app.game.views.game_views import mod_game
    from app.outdoor.views.outdoor_views import mod_outdoor

    # API Blueprints
    return [
        # Public
        mod_game,
        mod_outdoor,
        # Admin
        mod_admin_game,
        mod_admin_outdoor,
    ]


"""
DEFINE DB
"""
setup_db()


"""
BLUEPRINT REGISTER
"""
[flask_app.register_blueprint(blueprint)
 for blueprint in get_register_blueprints()]

# production = os.environ.get('PRODUCTION', False)
# if production:
#     setup_logging()
