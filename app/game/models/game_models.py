from app import db
from werkzeug.security import generate_password_hash, check_password_hash


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80))
    password = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
    score = db.Column(db.Integer())
    is_admin = db.Column(db.Boolean())

    def __init__(self, name, password, description='', score=0, is_admin=False):
        self.name = name
        self.password = password
        self.description = description
        self.score = score
        self.is_admin = is_admin

    def to_dict(self, password=False):
        base = {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'score': self.score,
            'is_admin': self.is_admin,
        }
        if password:
            base.update(dict(password=self.password))
        return base


class Question(db.Model):
    __tablename__ = 'question'

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(
        'user.id', onupdate='SET NULL', ondelete='CASCADE'
    ), nullable=True)
    question_text = db.Column(db.String(80))
    is_opened = db.Column(db.Boolean)
    user_answer = db.Column(db.String(80))

    def __init__(self, user_id, question_text):
        self.user_id = user_id
        self.question_text = question_text
        self.is_opened = False
        self.user_answer = None

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'question_text': self.question_text,
            'is_opened': self.is_opened,
            'user_answer': self.user_answer,
        }


class Choice(db.Model):
    __tablename__ = 'choice'

    id = db.Column(db.Integer(), primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey(
        'question.id', onupdate='SET NULL', ondelete='CASCADE'
    ), nullable=True)
    option = db.Column(db.String(80))
    text = db.Column(db.String(255))
    is_answer = db.Column(db.Boolean)

    def __init__(self, question_id, option, text, is_answer=False):
        self.question_id = question_id
        self.option = option
        self.text = text
        self.is_answer = is_answer

    def to_dict(self, show_answer=False):
        base = {
            'id': self.id,
            'question_id': self.question_id,
            'option': self.option,
            'text': self.text,
        }
        if show_answer:
            base.update(dict(is_answer=self.is_answer))
        return base
