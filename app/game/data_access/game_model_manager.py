from sqlalchemy import and_
from app import db
from app.game.models.game_models import (
    User,
    Question,
    Choice,
)
from app.bridge.decorators.db_exception import (
    db_exception
)
from app.bridge.constants.error import (
    OperationNotSupported
)

# ------------------------
# USER
# ------------------------
@db_exception
def get_user_by_password(password):
    return User.query.filter_by(password=password).first().to_dict()


@db_exception
def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first().to_dict()


@db_exception
def get_all_user():
    return [user.to_dict(True) for user in User.query.all()]


@db_exception
def update_user_field(user_id, data):
    user = User.query.filter_by(id=user_id)
    user.update(data)
    db.session.commit()
    return user.first().to_dict(True)


@db_exception
def create_user(name, password, description, score, is_admin):
    user = User(**locals())
    db.session.add(user)
    db.session.commit()
    return user.to_dict(True)


@db_exception
def delete_user_by_id(user_id):
    user = User.query.filter_by(id=user_id).first()
    db.session.delete(user)
    db.session.commit()
    return user.to_dict(True)


# ------------------------
# QUESTION
# ------------------------

@db_exception
def get_question(user_id, question_id):
    return Question.query.filter_by(user_id=user_id).filter_by(id=question_id).first().to_dict()


@db_exception
def get_all_question():
    return [q.to_dict() for q in Question.query.all()]


@db_exception
def update_question_field(question_id, data):
    question = Question.query.filter_by(
        id=question_id
    )
    question.update(data)
    db.session.commit()
    return question.first().to_dict()


@db_exception
def create_question(user_id, question_text):
    question = Question(**locals())
    db.session.add(question)
    db.session.commit()
    return question.to_dict()


@db_exception
def delete_question_by_id(question_id):
    question = Question.query.filter_by(id=question_id).first()
    db.session.delete(question)
    db.session.commit()
    return question.to_dict()

# ------------------------
# CHOICES
# ------------------------


@db_exception
def get_choices_by_question_id(question_id, show_answer=False):
    return [c.to_dict(show_answer) for c in Choice.query.filter_by(question_id=question_id)]


@db_exception
def create_choice(question_id, option, text, is_answer=False):
    choice = Choice(**locals())
    db.session.add(choice)
    db.session.commit()
    return choice.to_dict(True)


@db_exception
def update_choice_field(choice_id, data):
    choice = Choice.query.filter_by(
        id=choice_id
    )
    choice.update(data)
    db.session.commit()
    return choice.first().to_dict()
