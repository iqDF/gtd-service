from app.game.data_access import (
    game_model_manager
)
from app.bridge.constants.error import ServerOk, DatabaseError, OperationNotSupported

ALLOWED_USER_UPDATE_ATTR = [
    'name',
    'password',
    'score',
    'description',
    'is_admin'
]
ALLOWED_QUESTION_UPDATE_ATTR = [
    'user_id',
    'question_text',
    'is_opened',
    'user_answer'
]
ALLOWED_CHOICE_UPDATE_ATTR = [
    'option',
    'text',
    'is_answer',
]


def filter_field(fields, allowed_attr):
    return {k: v for k, v in fields.items(
    ) if v is not None and k in allowed_attr}


# ------------------------
# USER PUBLIC
# ------------------------


def get_user(password):
    user = game_model_manager.get_user_by_password(password)
    if isinstance(user, DatabaseError):
        return DatabaseError('User is not found'), {}
    if user['is_admin']:
        return OperationNotSupported('User is invalid'), {}
    return ServerOk(), user


# ------------------------
# USER ADMIN
# ------------------------


def get_user_admin(password):
    user = game_model_manager.get_user_by_password(password)
    if isinstance(user, DatabaseError):
        return DatabaseError('User is not found'), {}
    if user['is_admin']:
        return ServerOk(), user
    return OperationNotSupported('User is not admin'), {}


def get_user_admin_by_id(user_id):
    user = game_model_manager.get_user_by_id(user_id)
    if isinstance(user, DatabaseError):
        return user, {}
    if user['is_admin']:
        return ServerOk(), user
    return OperationNotSupported('User is not admin'), {}


def create_user(name, password, description, score, is_admin):
    user = game_model_manager.create_user(**locals())
    if isinstance(user, DatabaseError):
        return user, {}
    return ServerOk(), user


def get_user_list():
    user = game_model_manager.get_all_user()
    if isinstance(user, DatabaseError):
        return user, []
    return ServerOk(), user


def update_user_data(user_id, **fields):
    clean_field = filter_field(fields, ALLOWED_USER_UPDATE_ATTR)
    if len(clean_field.keys()) == 0:
        return OperationNotSupported('Fields cannot be empty'), {}
    user = game_model_manager.update_user_field(user_id, clean_field)
    if isinstance(user, DatabaseError):
        return user, {}
    return ServerOk(), user


def delete_user(user_id):
    user = game_model_manager.delete_user_by_id(user_id)
    if isinstance(user, DatabaseError):
        return user, 0
    return ServerOk(), user


# ------------------------
# QUESTION PUBLIC
# ------------------------


def get_question(user_id, question_id, api_access=True):
    question = game_model_manager.get_question(user_id, question_id)

    if isinstance(question, DatabaseError):
        return question, {}

    if api_access and question['is_opened']:
        return OperationNotSupported('Question has already been opened'), {}

    if api_access:
        update_status = game_model_manager.update_question_field(
            question_id,
            dict(is_opened=True)
        )
        if isinstance(update_status, DatabaseError):
            return update_status, {}

    choices = game_model_manager.get_choices_by_question_id(
        question_id, not api_access
    )
    question.update(dict(choices=choices))

    return ServerOk(), question


def get_choice_id(option, question_id):
    choices = game_model_manager.get_choices_by_question_id(question_id)
    for choice in choices:
        if choice['option'] == option:
            return choice['id']
    return None


def answer_question(user_id, question_id, option):
    user = game_model_manager.get_user_by_id(user_id=user_id)
    question = game_model_manager.get_question(
        question_id=question_id,
        user_id=user_id,
    )  # to check if the question wants to be answered is valid with the user_id

    if isinstance(question, DatabaseError):
        return question, user

    choice_id = get_choice_id(option, question_id)

    if not choice_id:
        return OperationNotSupported('Unable to obtain the choice'), user

    if question['user_answer']:
        return OperationNotSupported('Question has been answered'), user

    update_status = game_model_manager.update_question_field(
        question_id, dict(user_answer=choice_id))

    if isinstance(update_status, DatabaseError):
        return update_status, user

    correct = option in get_correct_choice(question_id)
    if correct:
        return ServerOk(), game_model_manager.update_user_field(user_id, dict(score=user['score']+1))
    return ServerOk(), user


# ------------------------
# QUESTION ADMIN
# ------------------------
def create_question(question_text, user_id, choices):
    question = game_model_manager.create_question(
        question_text=question_text,
        user_id=user_id
    )
    if isinstance(question, DatabaseError):
        return question, {}
    question_id = question['id']
    _choices = [game_model_manager.create_choice(
        question_id=question_id,
        **choice,
    ) for choice in choices]
    question.update(dict(choices=_choices))
    return ServerOk(), question


def get_question_list():
    question = game_model_manager.get_all_question()
    if isinstance(question, DatabaseError):
        return question, []
    for q in question:
        choices = game_model_manager.get_choices_by_question_id(q['id'], True)
        q.update(dict(choices=choices))
    return ServerOk(), question


def update_question_data(question_id, **fields):
    clean_field = filter_field(fields, ALLOWED_QUESTION_UPDATE_ATTR)
    if len(clean_field.keys()) == 0:
        return OperationNotSupported('Fields cannot be empty'), {}
    question = game_model_manager.update_question_field(
        question_id, clean_field
    )
    if isinstance(question, DatabaseError):
        return question, {}
    return ServerOk(), question


def get_correct_choice(question_id):
    question_choices = game_model_manager.get_choices_by_question_id(
        question_id,
        show_answer=True
    )
    return list(filter(lambda choice: choice, [
        choice['is_answer'] and choice['option'] for choice in question_choices
    ]))


def update_choice_list_data(question_id, choices):
    choices_id = [choice['id'] for choice in game_model_manager.get_choices_by_question_id(
        question_id
    )]
    for choice in choices:
        if choice['id'] in choices_id:
            update_choice_data(choice['id'], **choice)


def update_choice_data(choice_id, **fields):
    if not choice_id:
        return OperationNotSupported('Choice id cannot be blank'), {}
    clean_field = filter_field(fields, ALLOWED_CHOICE_UPDATE_ATTR)
    if len(clean_field.keys()) == 0:
        return OperationNotSupported('Fields cannot be empty'), {}
    choice = game_model_manager.update_choice_field(choice_id, clean_field)
    if isinstance(choice, DatabaseError):
        return choice, {}
    return ServerOk(), choice


def delete_question(question_id):
    question = game_model_manager.delete_question_by_id(question_id)
    if isinstance(question, DatabaseError):
        return question, 0
    return ServerOk(), question


def reset_question(question_id):
    param = dict(is_opened=False, user_answer=None)
    question = game_model_manager.update_question_field(question_id, param)
    choices = game_model_manager.get_choices_by_question_id(question_id, True)
    if isinstance(question, DatabaseError):
        return question, {}
    if isinstance(choices, DatabaseError):
        return choices, {}
    question.update(dict(choices=choices))
    return ServerOk(), question
