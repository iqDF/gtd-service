from flask import Blueprint, jsonify, request

from app.game.libs import (
    game_manager,
)

from app.game.views.game_validators import (
    user_login_validator,
    get_question_validator,
    answer_question_validator,
)

from app.bridge.constants.error import get_result_status_code, get_msg_error

mod_game = Blueprint('game', __name__)


# ------------------------
# USER
# ------------------------


@mod_game.route('/api/user/login', methods=['POST'])
@user_login_validator
def api_user_login():
    payload = dict(request.json)
    error_code, user = game_manager.get_user(
        password=payload['password'],
    )
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })


# ------------------------
# QUESTION
# ------------------------


@mod_game.route('/api/question', methods=['GET'])
@get_question_validator
def api_get_question():
    query = dict(request.args)
    error_code, question = game_manager.get_question(
        user_id=int(query['user_id'][0]),
        question_id=int(query['id'][0]),
    )
    return jsonify({
        **get_msg_error(error_code),
        'question': question,
    })


@mod_game.route('/api/question/<int:question_id>', methods=['POST'])
@answer_question_validator
def api_answer_question(question_id):
    payload = dict(request.json)
    error_code, user = game_manager.answer_question(
        question_id=question_id,
        user_id=int(payload['user_id']),
        option=payload['option'],
    )
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })
