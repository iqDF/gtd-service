from app.bridge.decorators.validators import request_validator, assert_key


@request_validator
def user_login_validator(payload):
    assert_key(payload, 'password', str)


@request_validator
def get_question_validator(query):
    assert_key(query, 'user_id', [str, int])
    assert_key(query, 'id', str)


@request_validator
def answer_question_validator(payload):
    assert_key(payload, 'user_id', [str, int])
    assert_key(payload, 'option', str)
