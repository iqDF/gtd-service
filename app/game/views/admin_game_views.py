from flask import Blueprint, jsonify, request

from app.game.libs import (
    game_manager,
)

from app.bridge.decorators.admin_required import admin_required

from app.game.views.admin_game_validators import (
    create_user_validator,
    update_user_validator,
    create_question_validator,
    update_question_validator,
)

from app.game.views.game_validators import user_login_validator

from app.bridge.constants.error import get_result_status_code, get_msg_error

mod_admin_game = Blueprint('admin_game', __name__)


# ------------------------
# USER
# ------------------------


@mod_admin_game.route('/api/admin/login', methods=['POST'])
@user_login_validator
def api_user_login():
    payload = dict(request.json)
    error_code, user = game_manager.get_user_admin(
        password=payload['password'],
    )
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })


@mod_admin_game.route('/api/user/create', methods=['POST'])
@admin_required
@create_user_validator
def api_create_user():
    payload = dict(request.json)
    error_code, user = game_manager.create_user(
        name=payload['name'],
        password=payload['password'],
        description=payload['description'],
        score=payload['score'],
        is_admin=payload['is_admin'],
    )
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })


@mod_admin_game.route('/api/user/<int:user_id>/update', methods=['PUT'])
@admin_required
@update_user_validator
def api_update_user(user_id):
    payload = dict(request.json)
    error_code, user = game_manager.update_user_data(
        user_id=user_id,
        **payload
    )
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })


@mod_admin_game.route('/api/user/list', methods=['GET'])
@admin_required
def api_get_user_list():
    error_code, user = game_manager.get_user_list()
    return jsonify({
        **get_msg_error(error_code),
        'user': user,
    })


@mod_admin_game.route('/api/user/<int:user_id>/delete', methods=['DELETE'])
@admin_required
def api_delete_user(user_id):
    error_code, user = game_manager.delete_user(user_id)
    return jsonify({
        **get_msg_error(error_code),
        'user': user
    })

# ------------------------
# QUESTION
# ------------------------


@mod_admin_game.route('/api/question/list', methods=['GET'])
@admin_required
def api_get_question_list():
    error_code, question = game_manager.get_question_list()
    return jsonify({
        **get_msg_error(error_code),
        'question': question,
    })


@mod_admin_game.route('/api/question/create', methods=['POST'])
@admin_required
@create_question_validator
def api_create_question():
    payload = dict(request.json)
    error_code, question = game_manager.create_question(
        user_id=int(payload['user_id']),
        question_text=payload['question_text'],
        choices=payload['choices'],
    )
    return jsonify({
        **get_msg_error(error_code),
        'question': question,
    })


@mod_admin_game.route('/api/question/<int:question_id>/update', methods=['PUT'])
@admin_required
@update_question_validator
def api_update_question(question_id):
    payload = dict(request.json)
    error_code, _temp_question = game_manager.update_question_data(
        question_id=question_id,
        **payload
    )
    if get_result_status_code(error_code) != 200:
        return jsonify({**get_msg_error(error_code)})
    game_manager.update_choice_list_data(
        question_id=question_id,
        choices=payload['choices']
    )
    error_code, question = game_manager.get_question(
        user_id=int(_temp_question['user_id']),
        question_id=question_id,
        api_access=False
    )
    return jsonify({
        **get_msg_error(error_code),
        'question': question,
    })


@mod_admin_game.route('/api/question/<int:question_id>/delete', methods=['DELETE'])
@admin_required
def api_delete_question(question_id):
    error_code, question = game_manager.delete_question(question_id)
    return jsonify({
        **get_msg_error(error_code),
        'question': question
    })


@mod_admin_game.route('/api/question/<int:question_id>/reset', methods=['PUT'])
@admin_required
def api_reset_question(question_id):
    error_code, question = game_manager.reset_question(question_id)
    return jsonify({
        **get_msg_error(error_code),
        'question': question
    })
