from app.bridge.decorators.validators import request_validator, assert_key


@request_validator
def create_user_validator(payload):
    assert_key(payload, 'name', str)
    assert_key(payload, 'password', str)
    assert_key(payload, 'description', str)


@request_validator
def update_user_validator(payload):
    assert_key(payload, 'name', str, True)
    assert_key(payload, 'password', str, True)
    assert_key(payload, 'description', str, True)
    assert_key(payload, 'score', int, True)


@request_validator
def create_question_validator(payload):
    assert_key(payload, 'user_id', [str, int])
    assert_key(payload, 'question_text', str)
    assert_key(payload, 'choices', list)
    for choice in payload['choices']:
        assert_key(choice, 'option', str)
        assert_key(choice, 'text', str)
        assert_key(choice, 'is_answer', bool, True)


@request_validator
def update_question_validator(payload):
    assert_key(payload, 'user_id', [str, int], True)
    assert_key(payload, 'question_text', str, True)
    assert_key(payload, 'choices', list, True)
    for choice in payload['choices']:
        assert_key(choice, 'option', str)
        assert_key(choice, 'text', str)
        assert_key(choice, 'is_answer', bool, True)
