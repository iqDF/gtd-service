#!/bin/bash

export DATABASE_URI=mysql+pymysql://root:root@127.0.0.1:3306/gtd # get your mysql url
export FLASK_ENV=development # dev mode
export DEBUG=True # dev mode
python run.py # available at 127.0.0.1:5000